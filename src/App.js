import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showOrHide: 'show',
      Refresh: 'Refresh',
    }
  }

  showOrHideClick() {
    if ('show' === this.state.showOrHide) {
      this.setState({
        showOrHide: 'hide',
      })
    } else {
      this.setState({
        showOrHide: 'show',
      })
    }
  }

  RefreshClick() {
    this.setState({
      showOrHide: 'show',
      Refresh: 'Refresh',
    })
  }

  render() {
    return (
      <div className='App'>
        <button onClick={this.showOrHideClick.bind(this)}>{this.state.showOrHide}</button>
        <button onClick={this.RefreshClick.bind(this)}>{this.state.Refresh}</button>
        {
          this.state.showOrHide === 'hide' && <TodoList/>
        }

      </div>
    );
  }
}

export default App;
