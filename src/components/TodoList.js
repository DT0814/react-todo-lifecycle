import React, { Component } from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      butName: '添加'
    };
    console.log('constructor');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate');
  }

  addItem() {
    let index = Number.parseInt(this.state.index) + 1;
    this.setState({
      index: index
    });
  }

  render() {
    console.log('render');
    let rows = [];
    for (var i = 1; i <= this.state.index; i++) {
      rows.push(<p key={i}>List Tittle {i}</p>);
    }

    return (
      <div>
        <button onClick={this.addItem.bind(this)}>{this.state.butName}</button>
        {rows}
      </div>
    )
      ;
  }
}

export default TodoList;

